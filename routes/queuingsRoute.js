const express = require("express");
const router = express.Router();
const Queuing = require("../models/queuingModel");
const Business = require("../models/businessModel");
const { v4: uuidv4 } = require("uuid");
const stripe = require("stripe")(
  "sk_test_Ou1w6LVt3zmVipDVJsvMeQsc"
);
router.post("/queuebusiness", async (req, res) => {
  const { token } = req.body;
  try {
    const security = req.header("Q-API-KEY");
    if (security != "dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT") {
      res.send("Unauthorized access");
      return res.status(400).json();
    }
    else {
      const customer = await stripe.customers.create({
        email: token.email,
        source: token.id,
      });

      const payment = await stripe.charges.create(
        {
          amount: req.body.totalAmount,
          currency: "sgd",
          customer: customer.id,
          receipt_email: token.email
        },
        {
          idempotencyKey: uuidv4(),

        }
      );

      if (payment) {
        req.body.transactionId = payment.source.id;
        const newqueuing = new Queuing(req.body);
        await newqueuing.save();
        const business = await Business.findOne({ _id: req.body.business });
        console.log(req.body.business);
        business.queuedTimeSlots.push(req.body.queuedTimeSlots);

        await business.save();
        res.send("Your queuing is successfull");
      } else {
        return res.status(400).json(error);
      }
    }
  } catch (error) {
    console.log(error);
    return res.status(400).json(error);
  }
});


router.get("/getallqueuings", async (req, res) => {

  try {
    const security = req.header("Q-API-KEY");
    if (security != "dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT") {
      res.send("Unauthorized access");
      return res.status(400).json();
    }
    else {
      const queuings = await Queuing.find().populate('business').populate('user')
      res.send(queuings)
    }
  } catch (error) {
    return res.status(400).json(error);
  }

});

router.post("/deletequeue", async (req, res) => {
  try {
    const security = req.header("Q-API-KEY");
    if (security != "dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT") {
      res.send("Unauthorized access");
      return res.status(400).json();
    }
    else {
      await Queuing.findOneAndDelete({ _id: req.body.queueid });

      res.send("Queue deleted successfully");
    }
  } catch (error) {
    return res.status(400).json(error);
  }
});


module.exports = router;
