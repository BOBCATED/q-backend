const express = require("express");
const router = express.Router();
const User = require("../models/userModel")
const sendEmail = require('../../BackEndRepo/models/sendEmailModel');


router.post("/login", async (req, res) => {
    const { username, password, role } = req.body;
    try {
        const security = req.header("Q-API-KEY");
        if (security != "dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT") {
            res.send("Unauthorized access");
            return res.status(400).json();
        }
        else {
            User.findOne({ username: username }).exec(function (error, users) {
                if (error) {
                    return res.status(400).json(error);
                } else if (!users) {
                    return res.status(400).json("Invalid credentials");
                } else {
                    users.comparePassword(password, function (matchError, isMatch) {
                        if (matchError) {
                            return res.status(400).json(error);
                        } else if (!isMatch) {
                            return res.status(400).json("Invalid credentials");
                        } else {
                            res.send(users);
                        }
                    })
                }
            })
        }
    } catch (error) {
        return res.status(400).json(error);
    }
});

router.post("/register", async (req, res) => {

    try {
        const { username, password, cpassword } = req.body;
        if (password != cpassword) {
            return res.status(400).json("Passwords does not match");
        }

        const security = req.header("Q-API-KEY");
        if (security != "dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT") {
            res.send("Unauthorized access");
            return res.status(400).json();
        }
        else {
            User.findOne({ username: username }).exec(async function (error, users) {
                if (error) {
                    return res.status(400).json(error);
                } else if (users) {
                    return res.status(400).json("Username already in use");
                } else if (!users) {
                    const newuser = new User(req.body)
                    await newuser.save()

                    // email new customer
                    if (newuser.role === 'CUSTOMER') {
                        try {
                            await sendEmail({
                                to: newuser.email,
                                subject: 'Q - Welcome to Q!',
                                html: `<h4>Hello! ${newuser.username}</h4>
                       <p>Thanks for registering as a ${newuser.role}! Hope you enjoy using our platform.</p>`
                            });
                        }
                        catch (error) {
                            // do nothing if fails to send email
                        }
                    }
                    else if (newuser.role === 'MERCHANT') // email new merchant
                    {
                        try {
                            await sendEmail({
                                to: newuser.email,
                                subject: 'Q - Welcome to Q!',
                                html: `<h4>Hello! ${newuser.username}</h4>
                       <p>Thanks for registering as a ${newuser.role}! We will contact you further to onboard your business.</p>`
                            });
                        }
                        catch (error) {
                            // do nothing if fails to send email
                        }
                    }
                    res.send('User registered successfully')
                }
            })
        }
    } catch (error) {
        return res.status(400).json(error);
    }

});

router.get("/getallusers", async (req, res) => {
    try {
        const security = req.header("Q-API-KEY");
        if (security != "dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT") {
            res.send("Unauthorized access");
            return res.status(400).json();
        }
        else {
            const users = await User.find();
            res.send(users);
        }
    } catch (error) {
        return res.status(400).json(error);
    }
});

router.post("/edituser", async (req, res) => {
    try {
        const security = req.header("Q-API-KEY");
        if (security != "dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT") {
            res.send("Unauthorized access");
            return res.status(400).json();
        }
        else {
            const user = await User.findOne({ _id: req.body._id });
            user.username = req.body.username;
            user.email = req.body.email;
            user.businessName = req.body.businessName;
            user.role = req.body.role;

            await user.save();

            res.send("User details updated successfully");
        }
    } catch (error) {
        return res.status(400).json(error);
    }
});

router.post("/deleteuser", async (req, res) => {
    try {
        const security = req.header("Q-API-KEY");
        if (security != "dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT") {
            res.send("Unauthorized access");
            return res.status(400).json();
        }
        else {
            await User.findOneAndDelete({ _id: req.body.userid });

            res.send("User deleted successfully");
        }
    } catch (error) {
        return res.status(400).json(error);
    }
});


module.exports = router