const express = require("express");
const router = express.Router();
const Business = require("../models/businessModel");

router.get("/getallbusinesses", async (req, res) => {
  try {
    const security = req.header("Q-API-KEY");
    if (security != "dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT") {
      res.send("Unauthorized access");
      return res.status(400).json();
    }
    else {
      const businesses = await Business.find();
      res.send(businesses);
    }
  } catch (error) {
    return res.status(400).json(error);
  }
});

router.post("/addbusiness", async (req, res) => {
  try {
    const security = req.header("Q-API-KEY");
    if (security != "dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT") {
      res.send("Unauthorized access");
      return res.status(400).json();
    }
    else {
      const newbusiness = new Business(req.body);
      await newbusiness.save();
      res.send("Business added successfully");
    }
  } catch (error) {
    return res.status(400).json(error);
  }
});

router.post("/editbusiness", async (req, res) => {
  try {
    const security = req.header("Q-API-KEY");
    if (security != "dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT") {
      res.send("Unauthorized access");
      return res.status(400).json();
    }
    else {
      const business = await Business.findOne({ _id: req.body._id });
      business.name = req.body.name;
      business.image = req.body.image;
      business.businessType = req.body.businessType;
      business.depositAmount = req.body.depositAmount;
      business.capacity = req.body.capacity;

      await business.save();

      res.send("Business details updated successfully");
    }
  } catch (error) {
    return res.status(400).json(error);
  }
});

router.post("/deletebusiness", async (req, res) => {
  try {
    const security = req.header("Q-API-KEY");
    if (security != "dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT") {
      res.send("Unauthorized access");
      return res.status(400).json();
    }
    else {
      await Business.findOneAndDelete({ _id: req.body.businessid });

      res.send("Business deleted successfully");
    }
  } catch (error) {
    return res.status(400).json(error);
  }
});

module.exports = router;
