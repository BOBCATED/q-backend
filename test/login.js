//During testing the environment variable should be set to test
process.env.NODE_ENV = 'test';


//Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const { expect } = require("chai");
const should = chai.should();
chai.use(chaiHttp);

//Login parent block
describe('Routes for login', () => {
    //Testing the login POST routes
    describe('/POST usersRoute', () => {
        it('Supposed to return VALID login attempt', (done) => {
            const userCredentials = {
                username : "tester1",
                password : "password123"
            }
            chai.request(server)
                .post('/api/users/login')
                .send(userCredentials)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('username').eq('tester1');
                    res.body.should.have.property('role').eq('MERCHANT');
                    done();
                });
        });
        it('Supposed to return INVALID password attempt', (done) => {
            const userCredentials = {
                username: "tester1",
                password: "password234"
            }
            chai.request(server)
                .post('/api/users/login')
                .send(userCredentials)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.to.be.json;
                    expect(res.body).to.equal('Invalid Credentials');
                    done();
                });
        });
        it('Supposed to return INVALID username attempt', (done) => {
            const userCredentials = {
                username: "tester2",
                password: "password123"
            }
            chai.request(server)
                .post('/api/users/login')
                .send(userCredentials)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.to.be.json;
                    expect(res.body).to.equal('Invalid Credentials');
                    done();
                });
        });
    });

});


//Register parent block
describe('Routes for Register', () => {
    //Testing the register POST routes
    describe('/POST usersRoute', () => {
        it('Supposed to return INVALID username register attempt', (done) => {
            const userCredentials = {
                username: "tester1",
                password: "password123",
                cpassword: "password123",
                email: "tester1@gmail.com",
                role: "MERCHANT"
            }
            chai.request(server)
                .post('/api/users/register')
                .send(userCredentials)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.to.be.json;
                    expect(res.body).to.equal('Username already in use');
                    done();
                });
        });
        it('Supposed to return INVALID confirm password attempt (Merchant)', (done) => {
            const userCredentials = {
                username: "tester2",
                password: "password234",
                cpassword: "password123",
                email: "tester2@gmail.com",
                role: "MERCHANT"
            }
            chai.request(server)
                .post('/api/users/register')
                .send(userCredentials)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.to.be.json;
                    expect(res.body).to.equal('Passwords does not match');
                    done();
                });
        });
        it('Supposed to return INVALID confirm password attempt (Customer)', (done) => {
            const userCredentials = {
                username: "tester2",
                password: "password234",
                cpassword: "password123",
                email: "tester2@gmail.com",
                role: "CUSTOMER"
            }
            chai.request(server)
                .post('/api/users/register')
                .send(userCredentials)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.to.be.json;
                    expect(res.body).to.equal('Passwords does not match');
                    done();
                });
        });
    });

});