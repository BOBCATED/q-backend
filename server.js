const express = require('express')
const app = express()
const port = process.env.PORT || 5001
const dbConnection = require('./db')
app.use(express.json())

app.use('/api/businesses/' , require('./routes/businessesRoute'))
app.use('/api/users/' , require('./routes/usersRoute'))
app.use('/api/queuings/' , require('./routes/queuingsRoute'))


const path = require('path')

if(process.env.NODE_ENV==='production')
{

    app.use('/' , express.static('/usr/share/nginx/html/build'))

    app.get('*' , (req , res)=>{

          res.sendFile(path.resolve(__dirname, '/usr/share/nginx/html/build/index.html'));

    })

}

app.get('/', (req, res) => res.send('Hello World !'))


 


app.listen(port, () => console.log(`Node JS Server Started in Port ${port}`))

module.exports = app;