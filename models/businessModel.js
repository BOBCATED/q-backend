const mongoose = require("mongoose");

const businessSchema = new mongoose.Schema({

    name: { type: String, required: true, unique: true },
    image: { type: String, required: true },
    capacity: { type: Number, required: true },
    businessType: { type: String, required: true },
    queuedTimeSlots: [
        {
            from: { type: String, required: true },
            to: { type: String, required: true }
        }
    ],
    depositAmount: { type: Number, required: true }
}, { timestamps: true }
)
const businessModel = mongoose.model('businesses', businessSchema)
module.exports = businessModel