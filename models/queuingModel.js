const mongoose = require("mongoose");

const queueSchema = new mongoose.Schema({


  business: { type: mongoose.Schema.Types.ObjectID, ref: 'businesses' },
  user: { type: mongoose.Schema.Types.ObjectID, ref: 'users' },
  queuedTimeSlots: {
    from: { type: String },
    to: { type: String }
  },
  totalHours: { type: Number },
  totalAmount: { type: Number },
  transactionId: { type: String },
  priorityRequired: { type: Boolean }
},
  { timestamps: true }
)

const queueModel = mongoose.model('queuings', queueSchema)

module.exports = queueModel