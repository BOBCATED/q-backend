const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

const userSchema = new mongoose.Schema({
     username: { type: String, required: true, unique: true },
     password: { type: String, required: true },
     role: { type: String, required: true },
     businessName: { type: String },
     email: {type: String, required: true }
})

//To hash password before saving in DB
userSchema.pre("save", function (next) {
     const user = this

     if (this.isModified("password") || this.isNew) {
          bcrypt.genSalt(10, function (saltError, salt) {
               if (saltError) {
                    return next(saltError)
               } else {
                    bcrypt.hash(user.password, salt, function (hashError, hash) {
                         if (hashError) {
                              return next(hashError)
                         }

                         user.password = hash
                         next()
                    })
               }
          })
     } else {
          return next()
     }
})

//To compare hashed password in DB with user input
userSchema.methods.comparePassword = function (password, callback) {
     bcrypt.compare(password, this.password, function (error, isMatch) {
          if (error) {
               return callback(error)
          } else {
               callback(null, isMatch)
          }
     })
}

const userModel = mongoose.model('users', userSchema)
module.exports = userModel