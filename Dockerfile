# Docker file for Q System Back End.

# Init argument 
ARG REPO=068549204303.dkr.ecr.us-east-1.amazonaws.com/nodebaseslim

# Get node base image
FROM ${REPO}:latest 

#Create a working directory
WORKDIR /Backend

# Copy all package.json files to work directory. 
COPY package*.json ./

# Install dependancies 
RUN npm install

# Copy all files from to work directory
COPY . .


# Expose port 5001 of the container
EXPOSE 5001

# Run the docker script 
CMD ["npm", "run", "start"]